package jschool.actor;

import java.util.function.BiFunction;

/**
 * ActorSystem is a factory for actors.
 */
public interface ActorSystem {
    /**
     * Spawns a new actor.
     *
     * @param function that processes incoming message into outgoing response
     * @param <I>      message/request type
     * @param <O>      response type
     * @return new actor instance
     */
    <I, O> Actor<I, O> actorFor(BiFunction<Actor, I, O> function);

    /**
     * Stop all actors and clear all resources. Method blocks until ActorSystem is stopped.
     */
    void shutdown();
}
