package jschool.actor;

/**
 * Factory for ActorSystem
 */
public final class Actors {
    // todo return your implementation of ActorSystem
    public static ActorSystem newActorSystem() {
        ActorSystem actorSystem = new MySuperActorSystem();
        return actorSystem;
    }

    private Actors() {
    }
}
