package jschool.actor;

import java.util.concurrent.*;
import java.util.function.BiFunction;

/**
 * Created by Student on 13.05.2015.
 */
public class MySuperActor<I, O> implements Actor<I, O> {

    private ExecutorService executorService = Executors.newSingleThreadExecutor();
    private Future<O> future;
    private Actor actorSender;
    private BiFunction<Actor, I, O> biFunction;

    public MySuperActor(BiFunction<Actor, I, O> biFunction) {
        this.biFunction = biFunction;
    }

    @Override
    public void tell(Object message) {
        if (!executorService.isShutdown()) {
            future = executorService.submit(() -> biFunction.apply(this, (I) message));
        } else {
            System.out.println("Message '" + message + "' could not be delivered!!!");
        }
    }

    @Override
    public void tell(Object message, Actor sender) {
        if (!executorService.isShutdown()) {
            actorSender = sender;
            future = executorService.submit(() -> {
                if ("".equals(message)) {
                    reply("Received message is empty. Repeat sending message.");
                }

                return biFunction.apply(this, (I) message);
            });
        } else {
            System.out.println("Message '" + message + "' could not be delivered!!!");
        }
    }

    @Override
    public Future ask(Object request) {
        if (!executorService.isShutdown()) {
            future = executorService.submit(() -> biFunction.apply(this, (I) request));

            return future;
        } else {
            System.out.println("Request '" + request + "' could not be executed!!!");
            return null;
        }
    }

    @Override
    public Future ask(Object request, Actor sender) {
        if (!executorService.isShutdown()) {
            actorSender = sender;
            future = executorService.submit(() -> {
                if ("".equals(request)) {
                    reply("Received request is empty. Repeat sending request.");
                }

                return biFunction.apply(this, (I) request);
            });

            return future;
        } else {
            System.out.println("Request '" + request + "' could not be executed!!!");
            return null;
        }
    }

    @Override
    public Future<?> stop() {
        Future<?> ftr = Executors.newSingleThreadExecutor().submit(() -> {
            try {
                future.get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        });

        executorService.shutdown();

        return ftr;
    }

    @Override
    public ActorSystem actorSystem() {
        return null;
    }

    @Override
    public void waitForMessagesProcessing() {
        try {
            future.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void reply(Object message) {
        if (actorSender != null) {
            actorSender.tell(message, this);
        }
    }
}
