package jschool.actor;

import java.util.concurrent.Future;

/**
 * An actor is a computational entity that, in response to a message it receives, can concurrently:
 * - send a finite number of messages to other actors;
 * - create a finite number of new actors;
 * - change its internal state.
 * Actor expects messages of a particular type <I> and responds with objects of type <O>.
 * Actor should process incoming messages in FIFO order.
 * All actor methods should return immediately (i.e. they should not block caller's thread).
 */
public interface Actor<I, O> {
    /**
     * Send a message to the actor. It makes no assumptions about response that actor generates.
     *
     * @param message to be sent
     */
    void tell(I message);

    /**
     * Send a message and specify the sender.
     *
     * @param message to be sent
     * @param sender  sender of the message
     */
    void tell(I message, Actor sender);

    /**
     * Sends a request to the actor and returns a future with the response
     *
     * @param request to be sent to the actor
     * @return response future
     */
    Future<O> ask(I request);

    /**
     * The same as method 'ask' above, but with additional sender parameter
     *
     * @param request to be sent to the actor
     * @param sender  sender of the message
     * @return response future
     */
    Future<O> ask(I request, Actor sender);

    /**
     * Stops the actor. Method is not blocking. It should return immediately.
     * All messages sent to the actor should be processed before actor is stopped.
     * Messages received after stop is called should be discarded.
     *
     * @return future that completes (future.get() completes) when actor is stopped.
     */
    Future<?> stop();

    ActorSystem actorSystem();

    void waitForMessagesProcessing();

    void reply(Object message);
}
