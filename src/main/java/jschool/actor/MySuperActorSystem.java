package jschool.actor;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;

/**
 * Created by Student on 13.05.2015.
 */
public class MySuperActorSystem implements ActorSystem {

    private List<Actor> actorList = new ArrayList<>();

    @Override
    public <I, O> Actor<I, O> actorFor(BiFunction<Actor, I, O> function) {
        Actor<I, O> actor = new MySuperActor<I, O>(function);

        actorList.add(actor);

        return actor;
    }

    @Override
    public void shutdown() {
        for (Actor actor : actorList) {
            actor.stop();
        }

        actorList.clear();
    }
}
