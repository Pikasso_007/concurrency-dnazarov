package jschool.actor;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.function.BiFunction;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

public class ActorsTest {

    private ActorSystem actorSystem;

    @Before
    public void setup() {
        actorSystem = Actors.newActorSystem();
    }

    @After
    public void tearDown() {
        actorSystem.shutdown();
    }

    @Test
    public void ask() throws ExecutionException, InterruptedException {
        Actor<String, String> actor = actorSystem.actorFor((sender, str) -> str.toUpperCase());
        Future<String> ask = actor.ask("ask");
        assertEquals("ASK", ask.get());
    }

    @Test
    public void tell() {
        @SuppressWarnings("unchecked")
        BiFunction<Actor, String, String> fun = mock(BiFunction.class);

        Actor<String, String> actor = actorSystem.actorFor(fun);
        actor.tell("one");
        actor.tell("two");
        actor.tell("three");

        actor.waitForMessagesProcessing();

        verify(fun).apply(any(), eq("one"));
        verify(fun).apply(any(), eq("two"));
        verify(fun).apply(any(), eq("three"));
        verifyNoMoreInteractions(fun);
    }

    @Test
    public void ordering() {
        @SuppressWarnings("unchecked")
        BiFunction<Actor, String, String> fun = mock(BiFunction.class);

        Actor<String, String> actor = actorSystem.actorFor(fun);
        actor.tell("one");
        actor.ask("two");
        actor.tell("three");
        actor.ask("four");

        actor.waitForMessagesProcessing();

        InOrder inOrder = inOrder(fun);
        inOrder.verify(fun).apply(any(), eq("one"));
        inOrder.verify(fun).apply(any(), eq("two"));
        inOrder.verify(fun).apply(any(), eq("three"));
        inOrder.verify(fun).apply(any(), eq("four"));

        verifyNoMoreInteractions(fun);
    }

    @Test
    public void stopping() {
        @SuppressWarnings("unchecked")
        BiFunction<Actor, String, String> fun = mock(BiFunction.class);
        Actor<String, String> actor = actorSystem.actorFor(fun);

        actor.tell("one");
        actor.tell("two");

        actor.stop();

        actor.tell("three");
        actor.tell("four");

        actor.waitForMessagesProcessing();

        verify(fun).apply(any(), eq("one"));
        verify(fun).apply(any(), eq("two"));
        verifyNoMoreInteractions(fun);
    }

    @Test
    public void testReply_behaviorActorWhenReceivedEmptyMessage_shouldInformSenderWithMessage() {
        Actor<String, String> actor1 = actorSystem.actorFor((sender, str) -> "");
        Actor<String, String> actor2 = mock(Actor.class);

        actor1.tell("", actor2);

        actor1.waitForMessagesProcessing();

        verify(actor2).tell(any(), eq(actor1));
    }
}